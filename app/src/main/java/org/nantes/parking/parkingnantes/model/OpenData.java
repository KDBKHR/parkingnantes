package org.nantes.parking.parkingnantes.model;

import android.view.ViewDebug;

import com.google.gson.annotations.Expose;

/**
 * Created by Khira on 11/03/2018.
 */

public class OpenData {

    @Expose
    public ParkingAnswer answer;

}
