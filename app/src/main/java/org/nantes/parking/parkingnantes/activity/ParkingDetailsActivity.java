package org.nantes.parking.parkingnantes.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

import org.nantes.parking.parkingnantes.R;
import org.nantes.parking.parkingnantes.model.ParkingDetails;
import org.nantes.parking.parkingnantes.service.ParkingDataBaseService;
import org.nantes.parking.parkingnantes.service.ParkingEventService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParkingDetailsActivity extends AppCompatActivity implements
        OnStreetViewPanoramaReadyCallback {

    @BindView(R.id.parking_details_name)
    TextView parkingNom;

    @BindView(R.id.parking_details_adress)
    TextView parkingAdresse;

    @BindView(R.id.parking_details_away_places)
    TextView parkingCapacite;

    @BindView(R.id.parking_details_favorites_button)
    ImageButton parkingFavButton;

    @BindView(R.id.credit_card_imageview)
    ImageView creditCardImageView;

    @BindView(R.id.money_imageview)
    ImageView cashImageView;

    @BindView(R.id.total_gr_imageview)
    ImageView totalGrImageView;

    @BindView(R.id.cheque_imageview)
    ImageView chequeImageView;

    @BindView(R.id.parking_details_ligne1_imageview)
    ImageView ligne1Img;

    @BindView(R.id.parking_details_ligne2_imageview)
    ImageView ligne2Img;

    @BindView(R.id.parking_details_ligne3_imageview)
    ImageView ligne3Img;

    @BindView(R.id.parking_details_ligne4_imageview)
    ImageView ligne4Img;

    ParkingDetails selectedParking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_details);
        ButterKnife.bind(this);
        final StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager().findFragmentById(R.id
                        .parking_details_mapView);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);
        this.selectedParking = (ParkingDetails) getIntent().getSerializableExtra("SelectedParking");
        setParkingFavoriteButton();
        setTextViewFields();
        setPaymentOptions();
		setPublicsTransports();
    }

    @OnClick(R.id.parking_details_favorites_button)
    public void clickOnAddParkingToFavorite() {
        final String id = this.selectedParking.getMainId();
        ParkingEventService.setFavoriteEnabled(id);
        setParkingFavoriteButton();
        setToastMessage();
    }

    private void setParkingFavoriteButton() {
        boolean isParkingFavorite = ParkingDataBaseService.isParkingFavorite(this.selectedParking);
        if (isParkingFavorite) {
            this.parkingFavButton.setImageResource(R.drawable.ic_40dp_yellow_star);
        } else {
            this.parkingFavButton.setImageResource(R.drawable.ic_40dp_star);
        }
    }

    private void setToastMessage() {
		boolean isParkingFavorite = ParkingDataBaseService.isParkingFavorite(this.selectedParking);
		String toastText =  "Supprimé des favoris";
		if (isParkingFavorite) {
			toastText = "Ajouté aux favoris";
		}
		Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT)
				.show();
	}

    private void setTextViewFields() {
        this.parkingNom.setText(this.selectedParking.getName());
        final String codePostal = String.valueOf(this.selectedParking.getZipCode());
        this.parkingAdresse.setText(this.selectedParking.getAddress() + " " + codePostal);
        final int capaciteTotale = this.selectedParking.getMaxPlaces();
        final int placesDisponibles = this.selectedParking.getAvailablePlaceInt();
        parkingCapacite.setText(placesDisponibles + "/" + capaciteTotale);
    }

    private void setPaymentOptions() {
        if (!this.selectedParking.isCreditCardAvailable()) {
            this.creditCardImageView.setVisibility(ImageView.GONE);
        }
        if (!this.selectedParking.isCashAvailable()) {
            this.cashImageView.setVisibility(ImageView.GONE);
        }
        if (!this.selectedParking.isTotalGRCardAvailable()) {
            this.totalGrImageView.setVisibility(ImageView.GONE);
        }
        if (!this.selectedParking.isChequeAvailable()) {
            this.chequeImageView.setVisibility(ImageView.GONE);
        }
    }

	private void setPublicsTransports() {
		if (!this.selectedParking.isLigneOneNear()) {
			this.ligne1Img.setVisibility(ImageView.GONE);
		}
		if (!this.selectedParking.isLigneTwoNear()) {
			this.ligne2Img.setVisibility(ImageView.GONE);
		}
		if (!this.selectedParking.isLigneThreeNear()) {
			this.ligne3Img.setVisibility(ImageView.GONE);
		}
		if (!this.selectedParking.isLigneFourNear()) {
			this.ligne4Img.setVisibility(ImageView.GONE);
		}
	}

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
        final double latitude = this.selectedParking.getLatitude();
        final double longitude = this.selectedParking.getLongitude();
        panorama.setPosition(new LatLng(latitude, longitude));
    }

    @OnClick(R.id.parking_details_go_path_button)
	public void onGoPathButtonClicked() {
		Intent intent = new Intent(this, ParkingPathActivity.class);
		intent.putExtra("SelectedParking", this.selectedParking);
		startActivity(intent);
	}

	@OnClick(R.id.toolbar_parking_list)
	public void onToolbarParkingListClick() {
		Intent intent = new Intent(this, ParkingActivity.class);
		startActivity(intent);
	}

	@OnClick(R.id.toolbar_parking_favorites)
	public void onToolbarParkingFavoritesClick() {
		Intent intent = new Intent(this, ParkingFavoritesActivity.class);
		startActivity(intent);
	}

	@OnClick(R.id.toolbar_map)
	public void onToolbarMapClick() {
		Intent intent = new Intent(this, ParkingMapActivity.class);
		startActivity(intent);
	}

}
