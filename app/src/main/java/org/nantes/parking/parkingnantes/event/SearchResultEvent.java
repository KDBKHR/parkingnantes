package org.nantes.parking.parkingnantes.event;

import org.nantes.parking.parkingnantes.model.ParkingDetails;

import java.util.ArrayList;
import java.util.List;

public class SearchResultEvent {

    private List<ParkingDetails> parkings;

    public SearchResultEvent(List<ParkingDetails> parkings) {
        this.parkings = parkings;
    }

    public List<ParkingDetails> getParkings() {
        return parkings;
    }
}
