package org.nantes.parking.parkingnantes.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khira on 11/03/2018.
 */

public class ParkingGroup {

    @Expose
    public List<ParkingDetails> Groupe_Parking;

}
