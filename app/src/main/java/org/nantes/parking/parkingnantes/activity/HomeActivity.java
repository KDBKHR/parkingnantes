package org.nantes.parking.parkingnantes.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.nantes.parking.parkingnantes.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.home_parking_list_textview)
    public void onClickedParkingsListButton() {
        Intent searchParkingIntent = new Intent(HomeActivity.this, ParkingActivity.class);
        startActivity(searchParkingIntent);
    }


    @OnClick(R.id.home_fav_parking_list_textview)
    public void onClickedFavoriteParkingsButton() {
        Intent favoriteParkingIntent = new Intent(HomeActivity.this, ParkingFavoritesActivity.class);
        startActivity(favoriteParkingIntent);
    }

    @OnClick(R.id.home_map_textview)
    public void onClickedMapParkingsButton() {
        Intent mapParkingIntent = new Intent(HomeActivity.this, ParkingMapActivity.class);
        startActivity(mapParkingIntent);
    }

}
