package org.nantes.parking.parkingnantes.utility;

import android.widget.ImageView;

/**
 * Created by Khira on 26/04/2018.
 */

public class ParkingUtilities {

	public static void setImageVisibility(final boolean isServiceAvailable, final ImageView
			imageView) {
		int result = ImageView.GONE;
		if (isServiceAvailable) {
			result = ImageView.VISIBLE;
		}
		imageView.setVisibility(result);
	}


}
