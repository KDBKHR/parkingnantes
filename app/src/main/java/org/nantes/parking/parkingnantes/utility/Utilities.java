package org.nantes.parking.parkingnantes.utility;

/**
 * Created by Khira on 18/04/2018.
 */

public final class Utilities {

    public static String convertNullToEmpty(final String string) {
        String result = string;
        if (string == null) {
            result = "";
        }
        return result;
    }

    public static String convertStringValueToBoolInt(final String string) {
        int result = 0;
        if (string.equals("true")) {
            result = 1;
        }
        return String.valueOf(result);
    }

}
