package org.nantes.parking.parkingnantes.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import org.nantes.parking.parkingnantes.R;
import org.nantes.parking.parkingnantes.event.EventBusManager;
import org.nantes.parking.parkingnantes.event.SearchResultEvent;
import org.nantes.parking.parkingnantes.model.ParkingDetails;
import org.nantes.parking.parkingnantes.service.ParkingDataBaseService;
import org.nantes.parking.parkingnantes.service.ParkingSearchService;
import org.nantes.parking.parkingnantes.ui.ParkingAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParkingActivity extends AppCompatActivity {

    @BindView(R.id.parking_list)
    ListView parkingListView;

    @BindView(R.id.parking_search_bar)
    EditText parkingSearchBar;

    @BindView(R.id.advanced_search_button)
    ImageButton advSearchBtn;

    @BindView(R.id.activity_parking_loader)
	ProgressBar progressBar;

    private ParkingAdapter parkingAdapter;
    private boolean isCurrentStateAdvSearch = false;

    private String parkNameSearchRes;
    private String parkAddrSearchRes;
    private String parkNbPlacesSearchRes;
    private String credCardSearchRes;
    private String cashSearchRes;
    private String totalGrSearchRes;
    private String chequeSearchRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking);

        ButterKnife.bind(this);

        this.parkingAdapter = new ParkingAdapter(this, new ArrayList<ParkingDetails>());
        this.parkingListView.setAdapter(this.parkingAdapter);
        this.parkingListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);

        final String isAdvancedSearch = getIntent().getStringExtra("IsAdvancedSearch");
        if (isAdvancedSearch != null && isAdvancedSearch.equals("advancedSearch")) {
            searchParkingsFromAdvancedSearch();
            this.isCurrentStateAdvSearch = true;
           advSearchBtn.setImageResource(R.drawable.ic_25dp_cancel);
        } else {
            ParkingSearchService.INSTANCE.searchParkingsFromAvailability();
            ParkingSearchService.INSTANCE.searchParkingsFromLocation();
        }

        this.parkingSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // While text is changing, hide list and show loader
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String searchResult = editable.toString();
				progressBar.setVisibility(View.VISIBLE);
                onSearchParkings(searchResult);
            }
        });

        this.parkingListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Intent parkingDetailsIntent = new Intent(ParkingActivity.this,
                        ParkingDetailsActivity.class);
                final ParkingDetails parkingDetails = (ParkingDetails) adapterView
                        .getItemAtPosition(i);
                parkingDetailsIntent.putExtra("SelectedParking", parkingDetails);
                startActivity(parkingDetailsIntent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        parkingAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void searchResult(final SearchResultEvent event) {
        this.parkingAdapter.clear();
        this.parkingAdapter.addAll(event.getParkings());
        this.progressBar.setVisibility(View.GONE);
    }

    private void onSearchParkings(final String searchResult) {
        final List<ParkingDetails> parkings;
        if (this.isCurrentStateAdvSearch) {
            parkings = ParkingDataBaseService
                    .searchParkingsFromAdvancedSearchBar(searchResult, parkNameSearchRes,
                            parkAddrSearchRes,
                            parkNbPlacesSearchRes, credCardSearchRes, cashSearchRes,
                            totalGrSearchRes, chequeSearchRes);
			this.progressBar.setVisibility(View.GONE);
        } else {
            parkings = ParkingDataBaseService
                    .searchParkingsFromName(searchResult);
        }
        EventBusManager.BUS.post(new SearchResultEvent(parkings));
    }

    @OnClick(R.id.advanced_search_button)
    public void onClickAdvancedSearchButton() {
        if (this.isCurrentStateAdvSearch) {
            this.isCurrentStateAdvSearch = false;
            final List<ParkingDetails> parkings = ParkingDataBaseService
                    .searchAllParkings();
            EventBusManager.BUS.post(new SearchResultEvent(parkings));
            advSearchBtn.setImageResource(R.drawable.ic_25dp_white_advanced_search);
			this.progressBar.setVisibility(View.GONE);
        } else {
            Intent intent = new Intent(ParkingActivity.this, ParkingAdvancedSearchActivity.class);
            startActivity(intent);
        }
    }

    private void searchParkingsFromAdvancedSearch() {
        parkNameSearchRes = getIntent().getStringExtra("ParkNameSearchRes");
        parkAddrSearchRes = getIntent().getStringExtra("ParkAddrSearchRes");
        parkNbPlacesSearchRes = getIntent().getStringExtra
                ("ParkNbPlacesSearchRes");
        credCardSearchRes = getIntent().getStringExtra("IsCreditCardSelected");
        cashSearchRes = getIntent().getStringExtra("IsCashSelected");
        totalGrSearchRes = getIntent().getStringExtra
                ("IsTotalGrSelected");
        chequeSearchRes = getIntent().getStringExtra
                ("IsChequeSelected");
        final List<ParkingDetails> parkings = ParkingDataBaseService
                .getParkingsFromAdvancedSearch(parkNameSearchRes, parkAddrSearchRes,
                        parkNbPlacesSearchRes, credCardSearchRes, cashSearchRes,
                        totalGrSearchRes, chequeSearchRes);
        EventBusManager.BUS.post(new SearchResultEvent(parkings));
        this.parkingAdapter.clear();
        this.parkingAdapter.addAll(parkings);
		this.progressBar.setVisibility(View.GONE);
    }

	@OnClick(R.id.toolbar_parking_list)
	public void onToolbarParkingListClick() {
		Intent intent = new Intent(this, ParkingActivity.class);
		startActivity(intent);
	}

    @OnClick(R.id.toolbar_parking_favorites)
	public void onToolbarParkingFavoritesClick() {
		Intent intent = new Intent(this, ParkingFavoritesActivity.class);
		startActivity(intent);
	}

	@OnClick(R.id.toolbar_map)
	public void onToolbarMapClick() {
		Intent intent = new Intent(this, ParkingMapActivity.class);
		startActivity(intent);
	}

}
