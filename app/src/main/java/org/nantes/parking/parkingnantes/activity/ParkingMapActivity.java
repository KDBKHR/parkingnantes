package org.nantes.parking.parkingnantes.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.activeandroid.query.Select;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;

import org.nantes.parking.parkingnantes.R;
import org.nantes.parking.parkingnantes.event.EventBusManager;
import org.nantes.parking.parkingnantes.event.SearchResultEvent;
import org.nantes.parking.parkingnantes.model.ParkingDetails;
import org.nantes.parking.parkingnantes.service.ParkingSearchService;
import org.nantes.parking.parkingnantes.ui.ParkingMapAdapter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParkingMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    @BindView(R.id.search_parking)
    EditText mSearchEditText;

    private Map<String, ParkingDetails> mMarkersToParking = new LinkedHashMap<>();
    private GoogleMap activeGoogleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_map);
        ButterKnife.bind(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        this.mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Nothing to do when texte is about to change
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // While text is changing, hide list and show loader
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String searchResult = editable.toString();
                onSearchParkings(searchResult);
            }
        });
    }

    @Override
    protected void onResume() {
        // Do NOT forget to call super.onResume()
        super.onResume();

        // Register to Event bus : now each time an event is posted, the activity will receive it if it is @Subscribed to this event
        EventBusManager.BUS.register(this);
        ParkingSearchService.INSTANCE.searchParkingsFromLocation();
    }

    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);

        // Do NOT forget to call super.onPause()
        super.onPause();
    }

    @Subscribe
    public void searchResult(final SearchResultEvent event) {
        if (this.activeGoogleMap != null) {
            this.activeGoogleMap.clear();
            this.mMarkersToParking.clear();
            for (ParkingDetails parking : event.getParkings()) {
                MarkerOptions markerOptions = new MarkerOptions()
                        .position(new LatLng(parking.getLatitude(), parking.getLongitude()));
                ParkingMapAdapter customInfoWindow = new ParkingMapAdapter(this);
                this.activeGoogleMap.setInfoWindowAdapter(customInfoWindow);
                Marker marker = this.activeGoogleMap.addMarker(markerOptions);
                this.mMarkersToParking.put(marker.getId(), parking);
                marker.setTag(parking);
                marker.showInfoWindow();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        activeGoogleMap = googleMap;
        activeGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        activeGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        float zoomLevel = 16.0f;
        final LatLng latLng = new LatLng(47.2131088858447, -1.55769456092052);
        activeGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
        activeGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                ParkingDetails parkingDetails = mMarkersToParking.get(marker.getId());
                if (parkingDetails != null) {
                    final Intent seeParkingDetailIntent = new Intent(ParkingMapActivity.this, ParkingDetailsActivity.class);
                    seeParkingDetailIntent.putExtra("SelectedParking", parkingDetails);
                    startActivity(seeParkingDetailIntent);
                }
            }
        });
    }

    private void onSearchParkings(final String searchResult) {
        List<ParkingDetails> matchingPlacesFromDB = new Select().
                from(ParkingDetails.class)
                .where("Grp_nom LIKE '%" + searchResult + "%'")
                .execute();
        EventBusManager.BUS.post(new SearchResultEvent(matchingPlacesFromDB));
    }

    @OnClick(R.id.toolbar_parking_list)
    public void onToolbarParkingListClick() {
        Intent intent = new Intent(this, ParkingActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.toolbar_parking_favorites)
    public void onToolbarParkingFavoritesClick() {
        Intent intent = new Intent(this, ParkingFavoritesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.toolbar_map)
    public void onToolbarMapClick() {
        Intent intent = new Intent(this, ParkingMapActivity.class);
        startActivity(intent);
    }

}