package org.nantes.parking.parkingnantes.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kboukhiar on 09/02/2018.
 */

@Table(name = "ParkingDetails")
public class ParkingDetails extends Model implements Serializable {

    @Expose
    @Column(name = "IdObj", index = true ,unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String IdObj;

    @Expose
    @Column(name = "_IDOBJ")
    public int _IDOBJ;

    @Expose
    @Column(name = "Grp_nom")
    public String Grp_nom;

    @Expose
    public ParkingGeo geo;

    @Expose
    @Column(name = "MOYEN_PAIEMENT")
    public String MOYEN_PAIEMENT;

    @Expose
    @Column(name = "ADRESSE")
    public String ADRESSE;

    @Expose
    @Column(name = "CODE_POSTAL")
    public int CODE_POSTAL;

    @Expose
    public String Grp_disponible;

    @Column(name = "PlaceDisponible")
    public int availablePlaces;

    @Expose
    @Column(name = "NbPlaceTotal")
    public int CAPACITE_VOITURE;

    @Expose
    public List<Double> _l = new ArrayList<Double>();

    @Expose
    @Column(name="TransportsCommuns")
    public String ACCES_TRANSPORTS_COMMUNS;

    @Column(name = "Latitude")
    public double latitude;

    @Column(name = "Longitude")
    public double longitude;

    @Column(name = "Favorite")
    public boolean isFavorite = false;

    @Column(name="CarteBancaire")
    public boolean isCreditCardAvailable = false;

    @Column(name="Espece")
    public boolean isCashAvailable = false;

    @Column(name="TotalGR")
    public boolean isTotalGRCardAvailable = false;

    @Column(name="Cheque")
    public boolean isChequeAvailable = false;

    @Column(name="LigneUne")
    public boolean isLigneOneNear = false;

    @Column(name="LigneTwo")
    public boolean isLigneTwoNear = false;

    @Column(name="LigneThree")
    public boolean isLigneThreeNear = false;

    @Column(name="LigneFour")
    public boolean isLigneFourNear = false;

    public String getMainId() {
        return this.IdObj;
    }

    public void setMainId(String mainId) {
        this.IdObj = mainId;
    }

    public int getSecondId() {
        return this._IDOBJ;
    }

    public void setSecondId(final int secondId) {
        this._IDOBJ = secondId;
    }

    public String getName() {
        return this.Grp_nom;
    }

    public void setName(final String name) {
        this.Grp_nom = name;
    }

    public ParkingGeo getGeo() {
        return this.geo;
    }

    public void setParkingGeo(final ParkingGeo geo) {
        this.geo = geo;
    }

    public String getPaymentOption() {
        return this.MOYEN_PAIEMENT;
    }

    public void setPaymentOption(final String payOpt) {
        this.MOYEN_PAIEMENT = payOpt;
    }

    public String getAddress() {
        return this.ADRESSE;
    }

    public void setAddress(final String address) {
        this.ADRESSE = address;
    }

    public int getZipCode() {
        return this.CODE_POSTAL;
    }

    public void setZipCode(final int zipCode) {
        this.CODE_POSTAL = zipCode;
    }

    public String getAvailablePlaces() {
        return this.Grp_disponible;
    }

    public void setAvailablePlaces(final String availablePlaces) {
        this.Grp_disponible = availablePlaces;
    }

    public int getAvailablePlaceInt() {
        return this.availablePlaces;
    }

    public void setAvailablePlaceInt(final int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }

    public int getMaxPlaces() {
        return this.CAPACITE_VOITURE;
    }

    public void setMaxPlaces(final int maxPlaces) {
        this.CAPACITE_VOITURE = maxPlaces;
    }

    public List<Double> getLocation() {
        return this._l;
    }

    public void setLocation(final List<Double> location) {
        this._l = location;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(final double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(final double longitude) {
        this.longitude = longitude;
    }

    public boolean isFavorite() {
        return this.isFavorite;
    }

    public void setFavorite(final boolean favorite) {
        this.isFavorite = favorite;
    }

    public boolean isCreditCardAvailable() {
        return this.isCreditCardAvailable;
    }

    public void setCreditCardAvailable(final boolean creditCardAvailable) {
        this.isCreditCardAvailable = creditCardAvailable;
    }

    public boolean isCashAvailable() {
        return this.isCashAvailable;
    }

    public void setCashAvailable(final boolean cashAvailable) {
        this.isCashAvailable = cashAvailable;
    }

    public boolean isTotalGRCardAvailable() {
        return this.isTotalGRCardAvailable;
    }

    public void setTotalGRCardAvailable(final boolean totalGRCardAvailable) {
        this.isTotalGRCardAvailable = totalGRCardAvailable;
    }

    public boolean isChequeAvailable() {
        return this.isChequeAvailable;
    }

    public void setChequeAvailable(final boolean chequeAvailable) {
        this.isChequeAvailable = chequeAvailable;
    }

    public String getTrasportsCommuns() {
        return ACCES_TRANSPORTS_COMMUNS;
    }

    public void setTrasportsCommuns(final String trasportsCommuns) {
        this.ACCES_TRANSPORTS_COMMUNS = trasportsCommuns;
    }

    public boolean isLigneOneNear() {
        return isLigneOneNear;
    }

    public void setLigneOneNear(final boolean ligneOneNear) {
        isLigneOneNear = ligneOneNear;
    }

    public boolean isLigneTwoNear() {
        return isLigneTwoNear;
    }

    public void setLigneTwoNear(final boolean ligneTwoNear) {
        isLigneTwoNear = ligneTwoNear;
    }

    public boolean isLigneThreeNear() {
        return isLigneThreeNear;
    }

    public void setLigneThreeNear(final boolean ligneThreeNear) {
        isLigneThreeNear = ligneThreeNear;
    }

    public boolean isLigneFourNear() {
        return isLigneFourNear;
    }

    public void setLigneFourNear(final boolean ligneFourNear) {
        isLigneFourNear = ligneFourNear;
    }

}
