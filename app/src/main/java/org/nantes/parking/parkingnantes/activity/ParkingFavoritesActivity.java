package org.nantes.parking.parkingnantes.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.squareup.otto.Subscribe;

import org.nantes.parking.parkingnantes.R;
import org.nantes.parking.parkingnantes.event.EventBusManager;
import org.nantes.parking.parkingnantes.event.SearchResultEvent;
import org.nantes.parking.parkingnantes.model.ParkingDetails;
import org.nantes.parking.parkingnantes.service.ParkingDataBaseService;
import org.nantes.parking.parkingnantes.ui.ParkingAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParkingFavoritesActivity extends AppCompatActivity {

    @BindView(R.id.parking_favorites_list)
    ListView parkingFavoritesListView;

    @BindView(R.id.parking_favorites_search_bar)
    EditText parkingFavoritesSearchBar;

    @BindView(R.id.activity_parking_favorites_loader)
	ProgressBar progressBar;

    private ParkingAdapter parkingFavoritesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_favorites);

        ButterKnife.bind(this);

        this.parkingFavoritesAdapter = new ParkingAdapter(this, new ArrayList<ParkingDetails>());
        this.parkingFavoritesListView.setAdapter(this.parkingFavoritesAdapter);
        this.parkingFavoritesListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        this.parkingFavoritesAdapter.addAll(ParkingDataBaseService.searchFavoritesParkings());

        parkingFavoritesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Intent parkingDetailsIntent = new Intent(ParkingFavoritesActivity.this,
                        ParkingDetailsActivity.class);
                final ParkingDetails parkingDetails = (ParkingDetails) adapterView
                        .getItemAtPosition(i);
                parkingDetailsIntent.putExtra("SelectedParking", parkingDetails);
                startActivity(parkingDetailsIntent);
            }
        });

        this.parkingFavoritesSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String searchResult = editable.toString();
				progressBar.setVisibility(View.VISIBLE);
                onSearchParkings(searchResult);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        final List<ParkingDetails> parkings = ParkingDataBaseService.searchFavoritesParkings();
        EventBusManager.BUS.post(new SearchResultEvent(parkings));
    }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void searchResult(final SearchResultEvent event) {
        this.parkingFavoritesAdapter.clear();
        this.parkingFavoritesAdapter.addAll(event.getParkings());
		progressBar.setVisibility(View.GONE);
    }

    private void onSearchParkings(final String searchResult) {
        final List<ParkingDetails> parkings = ParkingDataBaseService
                .searchFavoritesParkingsFromName(searchResult);
        EventBusManager.BUS.post(new SearchResultEvent(parkings));
    }

    @OnClick(R.id.toolbar_parking_list)
    public void onToolbarParkingListClick() {
        Intent intent = new Intent(this, ParkingActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.toolbar_parking_favorites)
    public void onToolbarParkingFavoritesClick() {
        Intent intent = new Intent(this, ParkingFavoritesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.toolbar_map)
    public void onToolbarMapClick() {
        Intent intent = new Intent(this, ParkingMapActivity.class);
        startActivity(intent);
    }


}
