package org.nantes.parking.parkingnantes.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Khira on 11/03/2018.
 */

public class ParkingAnswer {

    @Expose
    public ParkingData data;

}
