package org.nantes.parking.parkingnantes.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.nantes.parking.parkingnantes.R;
import org.nantes.parking.parkingnantes.model.ParkingDetails;
import org.nantes.parking.parkingnantes.utility.ParkingUtilities;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParkingAdapter extends ArrayAdapter<ParkingDetails> {

    @BindView(R.id.parking_adapter_name)
    TextView parkingName;

    @BindView(R.id.parking_adapter_address)
    TextView parkingAdress;

    @BindView(R.id.parking_adapter_nb_available_places)
    TextView parkingNbAvPlaces;

    @BindView(R.id.parking_adapter_nb_max_places)
    TextView parkingNbMaxPlaces;

    @BindView(R.id.parking_adapter_progressBar)
    ProgressBar parkingPlacesProgBar;

    @BindView(R.id.parking_adapter_credit_card_imageview)
    ImageView credCardImg;

    @BindView(R.id.parking_adapter_cash_imageview)
    ImageView cashImg;

    @BindView(R.id.parking_adapter_total_imageview)
    ImageView totalImg;

    @BindView(R.id.parking_adapter_cheque_imageview)
    ImageView chequeImg;

    @BindView(R.id.parking_adapter_fav_imageview)
    ImageView favortiteImg;

    public ParkingAdapter(Context context, List<ParkingDetails> places) {
        super(context, -1, places);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View actualView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context
                    .LAYOUT_INFLATER_SERVICE);
            actualView = inflater.inflate(R.layout.parking_adapter, parent, false);
        }
        ButterKnife.bind(this, actualView);
        ParkingDetails parkingDetails = getItem(position);

        if (parkingDetails != null) {
            this.parkingName.setText(parkingDetails.getName());
            final String address = parkingDetails.getAddress();
            if (address != null) {
                this.parkingAdress.setText(address);
            }
            int placeDisponible = parkingDetails.getAvailablePlaceInt();
            int placesMax = parkingDetails.getMaxPlaces();
            this.parkingNbAvPlaces.setText(String.valueOf(placeDisponible));
            this.parkingNbMaxPlaces.setText(String.valueOf(placesMax));
            this.parkingPlacesProgBar.setMax(100);
            if (placesMax != 0) {
                double percentage = getPercentage(placeDisponible, placesMax);
                this.parkingPlacesProgBar.setProgress((int) percentage);
            }
            setPaymentOptions(parkingDetails);
            ParkingUtilities.setImageVisibility(parkingDetails.isFavorite(), this.favortiteImg);
        }
        return actualView;
    }

    private double getPercentage(int currentValue, int refValue) {
        int value = refValue - currentValue;
        double div = ((double) value / (double) refValue);
        return (div * 100);
    }

    private void setPaymentOptions(final ParkingDetails parking) {
        ParkingUtilities.setImageVisibility(parking.isCreditCardAvailable(), this.credCardImg);
        ParkingUtilities.setImageVisibility(parking.isCashAvailable(), this.cashImg);
        ParkingUtilities.setImageVisibility(parking.isTotalGRCardAvailable(), this.totalImg);
        ParkingUtilities.setImageVisibility(parking.isChequeAvailable(), this.chequeImg);
    }

}
