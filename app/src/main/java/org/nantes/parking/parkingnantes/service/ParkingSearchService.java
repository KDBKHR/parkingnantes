package org.nantes.parking.parkingnantes.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.nantes.parking.parkingnantes.event.EventBusManager;
import org.nantes.parking.parkingnantes.event.SearchResultEvent;
import org.nantes.parking.parkingnantes.model.ParkingDetails;
import org.nantes.parking.parkingnantes.model.ParkingSearchResult;

import java.lang.reflect.Modifier;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by kboukhiar on 09/02/2018.
 */

public class ParkingSearchService {

    public static ParkingSearchService INSTANCE = new ParkingSearchService();
    private final ParkingRESTService parkingRestService;

    private final static String NANTES_API_URL = "https://data.nantes.fr/api/";

    private ParkingSearchService() {

        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl(NANTES_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        parkingRestService = retrofit.create(ParkingRESTService.class);
    }


    public void searchParkingsFromAvailability() {
        parkingRestService.searchParkingsFromAvailability().enqueue(new Callback<ParkingSearchResult>() {
            @Override
            public void onResponse(Call<ParkingSearchResult> call, Response<ParkingSearchResult>
                    response) {
                final ParkingSearchResult body = response.body();
                if (body != null && body.opendata.answer.data
                        .Groupes_Parking.Groupe_Parking != null) {
                    for (ParkingDetails parking : body.opendata.answer.data
                            .Groupes_Parking.Groupe_Parking) {
                        ParkingEventService.onSearchParkingFromAvailabilityAPI(parking);
                    }
                    searchParkingsFromDB();
                }
            }

            @Override
            public void onFailure(Call<ParkingSearchResult> call, Throwable t) {
                searchParkingsFromDB();
            }
        });
    }

    public void searchParkingsFromLocation() {
        parkingRestService.searchParkingsFromLocation().enqueue(new Callback<ParkingSearchResult>
                () {
            @Override
            public void onResponse(Call<ParkingSearchResult> call, Response<ParkingSearchResult>
                    response) {
                final ParkingSearchResult body = response.body();
                if (body != null && body.data != null) {
                    for (ParkingDetails parking : body.data) {
                        ParkingEventService.onSearchParkingFromLocationAPI(parking);
                    }
                    searchParkingsFromDB();
                }
            }

            @Override
            public void onFailure(Call<ParkingSearchResult> call, Throwable t) {
                searchParkingsFromDB();
            }
        });
    }

    public interface ParkingRESTService {

        @GET("getDisponibiliteParkingsPublics/1.0/39S4PNF4QF8KPG5/?output=json")
        Call<ParkingSearchResult> searchParkingsFromAvailability();

        @GET("publication/24440040400129_NM_NM_00044/LISTE_SERVICES_PKGS_PUB_NM_STBL/content" +
                "/?format=json")
        Call<ParkingSearchResult> searchParkingsFromLocation();
    }

    public void searchParkingsFromDB() {
        final List<ParkingDetails> parkings = ParkingDataBaseService
                .searchAllParkings();
        EventBusManager.BUS.post(new SearchResultEvent(parkings));
    }

}
