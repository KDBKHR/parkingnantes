package org.nantes.parking.parkingnantes.model;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by kboukhiar on 09/02/2018.
 */

public class ParkingSearchResult {

    @Expose
    public OpenData opendata;

    @Expose
    public List<ParkingDetails> data;

}
