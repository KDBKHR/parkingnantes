package org.nantes.parking.parkingnantes.service;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

import org.nantes.parking.parkingnantes.model.ParkingDetails;
import org.nantes.parking.parkingnantes.utility.Utilities;

import java.util.List;

/**
 * Created by Khira on 15/04/2018.
 */

public final class ParkingDataBaseService {

	public static List<ParkingDetails> searchAllParkings() {
		return new Select().
				from(ParkingDetails.class).orderBy("Grp_nom ASC").execute();
	}

	public static List<ParkingDetails> searchFavoritesParkings() {
		return new Select().
				from(ParkingDetails.class).where("Favorite LIKE '" + String.valueOf(1) + "'")
				.orderBy("Grp_nom ASC").execute();
	}

	public static boolean isParkingFavorite(ParkingDetails selectedParking) {
		final String id = selectedParking.getMainId();
		boolean isFavorite = false;
		ActiveAndroid.beginTransaction();
		final ParkingDetails parkingBD = new Select().
				from(ParkingDetails.class).where("IdObj LIKE '" + id + "'").executeSingle();
		if (parkingBD != null) {
			isFavorite = parkingBD.isFavorite();
		}
		ActiveAndroid.setTransactionSuccessful();
		ActiveAndroid.endTransaction();
		return isFavorite;
	}

	public static List<ParkingDetails> getParkingsFromAdvancedSearch(final String name,
																	 final String address,
																	 final String nbPlaces,
																	 final String
																			 isCredCardSelected,
																	 final String isCashSelected,
																	 final String
																			 isTotalGrSelected,
																	 final String
																			 isChequeSelected) {
		String nbPlacesResult = nbPlaces;
		if (nbPlaces == null || nbPlaces.equals("")) {
			nbPlacesResult = "1";
		}
		return new Select().
				from(ParkingDetails.class).where("PlaceDisponible >= '" + nbPlacesResult + "' AND " +
				"" + "" + "" + "" + "ADRESSE LIKE '%" + address + "%' AND Grp_nom LIKE '%" + name
				+ "%'" + " AND " + "(" + "CarteBancaire LIKE '" + Utilities
				.convertStringValueToBoolInt(isCredCardSelected) + "' OR Espece LIKE '" +
				Utilities.convertStringValueToBoolInt(isCashSelected) + "' OR TotalGR LIKE '" +
				Utilities.convertStringValueToBoolInt(isTotalGrSelected) + "' OR Cheque LIKE '" +
				Utilities.convertStringValueToBoolInt(isChequeSelected) + "')").orderBy("Grp_nom " +
				"ASC").execute();

	}

	public static List<ParkingDetails> searchParkingsFromAdvancedSearchBar(final String
																				   searchBarRes,
																		   final String name,
																		   final String address,
																		   final String nbPlaces,
																		   final String
																				   isCredCardSelected,
																		   final String
																				   isCashSelected,
																		   final String
																				   isTotalGrSelected,
																		   final String
																				   isChequeSelected) {
		String nbPlacesResult = nbPlaces;
		if (nbPlaces == null || nbPlaces.equals("")) {
			nbPlacesResult = "1";
		}
		return new Select().
				from(ParkingDetails.class).where("PlaceDisponible >= '" + nbPlacesResult + "' AND " +
				"" + "" + "" + "" + "ADRESSE LIKE '%" + address + "%' AND Grp_nom LIKE '%" + name
				+ "%'" + " AND " + "" + "Grp_nom " + "LIKE '%" + searchBarRes + "%' AND (" +
				"CarteBancaire" + " LIKE '" + Utilities.convertStringValueToBoolInt
				(isCredCardSelected) + "' OR " + "Espece LIKE '" + Utilities
				.convertStringValueToBoolInt(isCashSelected) + "' OR " + "TotalGR LIKE '" +
				Utilities.convertStringValueToBoolInt(isTotalGrSelected) + "' " + "OR Cheque LIKE " +
				"'" + Utilities.convertStringValueToBoolInt(isChequeSelected) + "')").orderBy
				("Grp_nom ASC").execute();
	}

	public static ParkingDetails searchParkingFromMainId(final String id) {
		return new Select().
				from(ParkingDetails.class).where("IdObj LIKE '" + id + "'").executeSingle();
	}

	public static List<ParkingDetails> searchParkingsFromName(final String name) {
		return new Select().
				from(ParkingDetails.class).where("Grp_nom LIKE '%" + name + "%'").orderBy
				("Grp_nom" + " ASC").execute();
	}

	public static List<ParkingDetails> searchFavoritesParkingsFromName(final String name) {
		return new Select().
				from(ParkingDetails.class).where("Grp_nom LIKE '%" + name + "%' AND Favorite LIKE " +
				"" + "" + "" + "" + "'" + String.valueOf(1) + "'").orderBy("Grp_nom ASC")
				.execute();
	}

}
