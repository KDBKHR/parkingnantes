package org.nantes.parking.parkingnantes.service;

import com.activeandroid.ActiveAndroid;

import org.nantes.parking.parkingnantes.model.ParkingDetails;
import org.nantes.parking.parkingnantes.utility.ParkingPaymentOptionsParser;
import org.nantes.parking.parkingnantes.utility.Utilities;

/**
 * Created by Khira on 23/04/2018.
 */

public final class ParkingEventService {


    private static void setParkingDetailsFromLocalisationAPI(final ParkingDetails localParking,
                                                             final ParkingDetails apiParking) {
        ActiveAndroid.beginTransaction();
        localParking.setAddress(apiParking.getAddress());
        localParking.setZipCode(apiParking.getZipCode());
        localParking.setMaxPlaces(apiParking.getMaxPlaces());
        localParking.setPaymentOption(apiParking.getPaymentOption());
        final String paymentOptions = Utilities.convertNullToEmpty(apiParking.getPaymentOption());
        localParking.setCashAvailable(ParkingPaymentOptionsParser.isCashAvailable(paymentOptions));
        localParking.setCreditCardAvailable(ParkingPaymentOptionsParser.isCreditCardAvailable
                (paymentOptions));
        localParking.setTotalGRCardAvailable(ParkingPaymentOptionsParser.isTotalGRAvailable
                (paymentOptions));
        localParking.setChequeAvailable(ParkingPaymentOptionsParser.isChequeAvailable
                (paymentOptions));
        localParking.setLatitude(apiParking.getLocation().get(0));
        localParking.setLongitude(apiParking.getLocation().get(1));
        final String publicsTransports = Utilities.convertNullToEmpty(apiParking
                .getTrasportsCommuns());
        localParking.setLigneOneNear(ParkingPaymentOptionsParser.isLigneOneNear(publicsTransports));
        localParking.setLigneTwoNear(ParkingPaymentOptionsParser.isLigneTwoNear(publicsTransports));
        localParking.setLigneThreeNear(ParkingPaymentOptionsParser.isLigneThreeNear
                (publicsTransports));
        localParking.setLigneFourNear(ParkingPaymentOptionsParser.isLigneFourNear
                (publicsTransports));
        localParking.save();
        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();
    }

    public static void onSearchParkingFromLocationAPI(final ParkingDetails apiParking) {
        final String id = String.valueOf(apiParking.getSecondId());
        final ParkingDetails parkingBD = ParkingDataBaseService.searchParkingFromMainId(id);
        ParkingDetails localParking = parkingBD;
        if (localParking == null) {
            localParking = apiParking;
            final String secondId = String.valueOf(apiParking.getSecondId());
            apiParking.setMainId(secondId);
            apiParking.setName(apiParking.getGeo().getName());
        }
        ParkingEventService.setParkingDetailsFromLocalisationAPI(localParking, apiParking);
    }

    public static void onSearchParkingFromAvailabilityAPI(final ParkingDetails apiParking) {
        ActiveAndroid.beginTransaction();
        final String id = apiParking.getMainId();
        final ParkingDetails parkingBD = ParkingDataBaseService.searchParkingFromMainId(id);
        ParkingDetails localParking = parkingBD;
        if (localParking != null) {
            localParking.setMainId(apiParking.getMainId());
            localParking.setName(apiParking.getName());
            final int availablePlaces = Integer.parseInt(apiParking.getAvailablePlaces());
            localParking.setAvailablePlaceInt(availablePlaces);
        } else {
            localParking = apiParking;
        }
        localParking.save();
        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();
    }

    public static void setFavoriteEnabled(final String id) {
        final ParkingDetails parking = ParkingDataBaseService.searchParkingFromMainId(id);
        if (parking != null) {
            ActiveAndroid.beginTransaction();
            final boolean favEnable = !parking.isFavorite();
            parking.setFavorite(favEnable);
            parking.save();
            ActiveAndroid.setTransactionSuccessful();
            ActiveAndroid.endTransaction();
        }
    }

}
