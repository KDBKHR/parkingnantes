package org.nantes.parking.parkingnantes.ui;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import org.nantes.parking.parkingnantes.R;
import org.nantes.parking.parkingnantes.model.ParkingDetails;
import org.nantes.parking.parkingnantes.utility.ParkingUtilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParkingMapAdapter implements GoogleMap.InfoWindowAdapter {

	private Context context;

	@BindView(R.id.parking_map_name)
	TextView parkingName;

	@BindView(R.id.parking_map_away_places)
	TextView parkingPlaces;

	@BindView(R.id.parking_map_adress)
	TextView parkingAddress;

	@BindView(R.id.parking_map_favorites_button)
	ImageView favImg;

	public ParkingMapAdapter(Context ctx){
		context = ctx;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		return null;
	}

	@Override
	public View getInfoContents(Marker marker) {

		View view = ((Activity)context).getLayoutInflater()
				.inflate(R.layout.parking_map_adapter, null);
		ButterKnife.bind(this,view);
		final ParkingDetails parking  = (ParkingDetails) marker.getTag();
		parkingName.setText(parking.getName());
		parkingAddress.setText(parking.getAddress());
		parkingPlaces.setText(String.valueOf(parking.getAvailablePlaceInt()));
		ParkingUtilities.setImageVisibility(parking.isFavorite(), this.favImg);
		return view;
	}
}
