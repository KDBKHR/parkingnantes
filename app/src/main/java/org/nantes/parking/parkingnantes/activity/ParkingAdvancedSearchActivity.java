package org.nantes.parking.parkingnantes.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import org.nantes.parking.parkingnantes.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Khira on 19/04/2018.
 */

public class ParkingAdvancedSearchActivity extends AppCompatActivity {

    @BindView(R.id.parking_name_textinput)
    TextInputLayout parkNameInput;

    @BindView(R.id.parking_address_textinput)
    TextInputLayout parkAddrInput;

    @BindView(R.id.adv_search_wanted_places)
    TextInputEditText parkPlacesNumPicker;

    @BindView(R.id.adv_search_submit_button)
    Button submitButton;

    @BindView(R.id.adv_search_card_button)
    ImageButton creditCardButton;

    @BindView(R.id.adv_search_cash_button)
    ImageButton cashButton;

    @BindView(R.id.adv_search_total_button)
    ImageButton totalGrButton;

    @BindView(R.id.adv_search_cheque_button)
    ImageButton chequeButton;

    private boolean isCredCardSelected = false;
    private boolean isCashSelected = false;
    private boolean isTotalGrSelected = false;
    private boolean isChequeSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advanced_search);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @OnClick(R.id.adv_search_submit_button)
    public void onClickSubmit() {
        final String parkNameSearchRes = this.parkNameInput.getEditText().getText().toString();
        final String parkAddrSearchRes = this.parkAddrInput.getEditText().getText().toString();
        final String parkNbPlacesSearchRes = this.parkPlacesNumPicker.getText().toString();
        final Intent parkingListIntent = new Intent(this, ParkingActivity.class);
        parkingListIntent.putExtra("ParkNameSearchRes", parkNameSearchRes);
        parkingListIntent.putExtra("ParkAddrSearchRes", parkAddrSearchRes);
        parkingListIntent.putExtra("ParkNbPlacesSearchRes", parkNbPlacesSearchRes);
        parkingListIntent.putExtra("IsCreditCardSelected", String.valueOf(this.isCredCardSelected));
        parkingListIntent.putExtra("IsCashSelected", String.valueOf(this.isCashSelected));
        parkingListIntent.putExtra("IsTotalGrSelected", String.valueOf(this.isTotalGrSelected));
        parkingListIntent.putExtra("IsChequeSelected", String.valueOf(this.isChequeSelected));
        parkingListIntent.putExtra("IsAdvancedSearch", "advancedSearch");
        startActivity(parkingListIntent);
    }

    @OnClick(R.id.adv_search_card_button)
    public void onCreditCardImageViewClicked() {
        this.isCredCardSelected = setImageButton(this.isCredCardSelected, this.creditCardButton,
                R.drawable.ic_30dp_credit_card, R.drawable.ic_30dp_credit_card_monochrom);
    }

    @OnClick(R.id.adv_search_cash_button)
    public void onCashImageViewClicked() {
        this.isCashSelected = setImageButton(this.isCashSelected, this.cashButton, R.drawable
                .ic_30dp_money, R.drawable.ic_30dp_money_monochrom);
    }

    @OnClick(R.id.adv_search_total_button)
    public void onTotalImageViewClicked() {
        this.isTotalGrSelected = setImageButton(this.isTotalGrSelected, this.totalGrButton, R
                .drawable.ic_30dp_total_color, R.drawable.ic_30dp_total);
    }

    @OnClick(R.id.adv_search_cheque_button)
    public void onChequeImageViewClicked() {
        this.isChequeSelected = setImageButton(this.isChequeSelected, this.chequeButton, R
                .drawable.ic_30dp_cheque_color, R.drawable.ic_30dp_cheque_monochrom);
    }

    private boolean setImageButton(boolean optionSelected,
                                   ImageButton imageButton,
                                   int imageSelected,
                                   int imageUnselected) {
        if (!optionSelected) {
            imageButton.setImageResource(imageSelected);
            optionSelected = true;
        } else {
            imageButton.setImageResource(imageUnselected);
            optionSelected = false;
        }
        return optionSelected;
    }

}
