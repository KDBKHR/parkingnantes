package org.nantes.parking.parkingnantes.model;

import com.google.gson.annotations.Expose;

/**
 * Created by kboukhiar on 09/02/2018.
 */

public class ParkingGeo {

    @Expose
    public String name;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
