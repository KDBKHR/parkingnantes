package org.nantes.parking.parkingnantes.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;
import org.nantes.parking.parkingnantes.R;
import org.nantes.parking.parkingnantes.model.ParkingDetails;
import org.nantes.parking.parkingnantes.utility.DirectionsParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import butterknife.ButterKnife;

/**
 * Code inspiré : https://pastebin.com/HWw9P86q et http://wptrafficanalyzer
 * .in/blog/driving-route-from-my-location-to-destination-in-google-maps-android-api-v2/
 */

public class ParkingPathActivity extends FragmentActivity implements OnMapReadyCallback,
		LocationListener {

	private GoogleMap googleMap;

	private final static int LOCATION_REQUEST = 500;
	private ParkingDetails selectedParking;

	ArrayList<LatLng> mMarkerPoints;
	double mLatitude = 0;
	double mLongitude = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parking_path);
		ButterKnife.bind(this);
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.patking_path_map);
		mapFragment.getMapAsync(this);
		this.selectedParking = (ParkingDetails) getIntent().getSerializableExtra
				("SelectedParking");
		mMarkerPoints = new ArrayList<>();
		showAlertDialog();
	}

	private void showAlertDialog() {
		AlertDialog.Builder builder;
		builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
		builder.setTitle("Localisation GPS").setMessage("Veuillez activer la géolocalisation")
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

			}
		}).setIcon(android.R.drawable.ic_dialog_alert).show();
	}

	@Override
	public void onMapReady(final GoogleMap googleMap) {
		this.googleMap = googleMap;
		this.googleMap.getUiSettings().setZoomControlsEnabled(true);
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
				PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
					.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
		} else {
			this.googleMap.setMyLocationEnabled(true);
			getPath();
		}
	}

	/**
	 * @SuppressLint("MissingPermission") : La vérification de la permission est effectuée dans
	 * les méthodes appelantes
	 */
	@SuppressLint("MissingPermission")
	private void getPath() {
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String provider = locationManager.getBestProvider(criteria, true);
		Location location = locationManager.getLastKnownLocation(provider);
		if (location != null) {
			onLocationChanged(location);
		}
		if (mMarkerPoints.size() > 1) {
			mMarkerPoints.clear();
			getGoogleMap().clear();
			LatLng startPoint = new LatLng(mLatitude, mLongitude);
			drawMarker(startPoint);
		}
		if (selectedParking != null) {
			final LatLng point = new LatLng(this.selectedParking.getLatitude(), this
					.selectedParking.getLongitude());
			drawMarker(point);
		}
		if (mMarkerPoints.size() >= 2) {
			LatLng origin = mMarkerPoints.get(0);
			LatLng dest = mMarkerPoints.get(1);
			String url = getRequestUrl(origin, dest);
			TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
			taskRequestDirections.execute(url);
		}
	}

	private void drawMarker(LatLng point) {
		mMarkerPoints.add(point);
		MarkerOptions options = new MarkerOptions();
		options.position(point);
		if (mMarkerPoints.size() == 1) {
			options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
		} else if (mMarkerPoints.size() == 2) {
			options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
		}
		googleMap.addMarker(options);
	}

	@Override
	public void onLocationChanged(final Location location) {
		if (mMarkerPoints.size() < 2) {
			mLatitude = location.getLatitude();
			mLongitude = location.getLongitude();
			LatLng point = new LatLng(mLatitude, mLongitude);
			googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
			googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
			drawMarker(point);
		}
	}

	@Override
	public void onStatusChanged(final String provider, final int status, final Bundle extras) {

	}

	@Override
	public void onProviderEnabled(final String provider) {

	}

	@Override
	public void onProviderDisabled(final String provider) {

	}

	private String requestDirection(String reqUrl) throws IOException {
		String responseString = "";
		InputStream inputStream = null;
		HttpsURLConnection httpsURLConnection = null;
		try {
			URL url = new URL(reqUrl);
			httpsURLConnection = (HttpsURLConnection) url.openConnection();
			httpsURLConnection.connect();

			inputStream = httpsURLConnection.getInputStream();
			final InputStreamReader inputStreamReader = new InputStreamReader((inputStream));
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line);
			}
			responseString = stringBuffer.toString();
			bufferedReader.close();
			inputStreamReader.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			httpsURLConnection.disconnect();
		}
		return responseString;
	}

	protected final String getRequestUrl(final LatLng origin, final LatLng dest) {
		String str_org = "origin=" + origin.latitude + "," + origin.longitude;
		String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
		String sensor = "sensor=false";
		String mode = "mode-driving";
		String param = str_org + "&" + str_dest + "&" + sensor + "&" + mode;
		String output = "json";
		String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param;
		return url;

	}

	@SuppressLint("MissingPermission")
	@Override
	public void onRequestPermissionsResult(final int requestCode,
										   @NonNull final String[] permissions,
										   @NonNull final int[] grantResults) {
		switch (requestCode) {
			case LOCATION_REQUEST:
				if (grantResults.length > 0 && grantResults[0] == PackageManager
						.PERMISSION_GRANTED) {
					this.googleMap.setMyLocationEnabled(true);
					getPath();
				}
				break;
		}
	}

	public GoogleMap getGoogleMap() {
		return this.googleMap;
	}

	public class TaskRequestDirections extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... strings) {
			String responseString = "";
			try {
				responseString = requestDirection(strings[0]);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return responseString;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);
			TaskParser taskParser = new TaskParser();
			taskParser.execute(s);
		}
	}

	public class TaskParser extends AsyncTask<String, Void, List<List<HashMap<String, String>>>> {

		@Override
		protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
			JSONObject jsonObject = null;
			List<List<HashMap<String, String>>> routes = null;
			try {
				jsonObject = new JSONObject(strings[0]);
				DirectionsParser directionsParser = new DirectionsParser();
				routes = directionsParser.parse(jsonObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return routes;
		}

		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
			List points = new ArrayList();
			PolylineOptions polylineOptions = null;
			for (List<HashMap<String, String>> path : lists) {
				points = new ArrayList();
				polylineOptions = new PolylineOptions();
				for (HashMap<String, String> point : path) {
					double lat = Double.parseDouble(point.get("lat"));
					double lon = Double.parseDouble(point.get("lon"));
					points.add(new LatLng(lat, lon));
				}
				polylineOptions.addAll(points);
				polylineOptions.width(15);
				polylineOptions.color(Color.BLUE);
				polylineOptions.geodesic(true);
			}
			if (polylineOptions != null) {
				getGoogleMap().addPolyline(polylineOptions);
			} else {
				Toast.makeText(getApplicationContext(), "Chemin non trouvé", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

}

